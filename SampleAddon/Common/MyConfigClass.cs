﻿using YamlDotNet.Serialization;

namespace SampleAddon.Common
{
    public class MyConfigClass
    {
        [YamlMember(Alias = "enabled")]
        public bool Enabled { get; set; }
    }
}
