﻿using IOTLinkAPI.Addons;
using IOTLinkAPI.Helpers;
using IOTLinkAPI.Platform;
using IOTLinkAPI.Platform.Events;
using SampleAddon.Common;
using System.Dynamic;
using System.IO;

namespace SampleAddon
{
    /// <summary>
    /// This class is used to create an interface with the IOTLink - Windows Service.
    /// Here you are going to transmit and receive messages from MQTT and also execute
    /// any method call which you can do without needing of a logged in user.
    /// </summary>
    public class SampleAddonService : ServiceAddon
    {
        private string _configPath;
        private MyConfigClass _config;

        public override void Init(IAddonManager addonManager)
        {
            base.Init(addonManager);

            _configPath = Path.Combine(this._currentPath, "config.yaml");
            ConfigHelper.SetReloadHandler<MyConfigClass>(_configPath, OnConfigReload);

            _config = ConfigHelper.GetConfiguration<MyConfigClass>(_configPath);

            // Handlers
            OnSessionChangeHandler += OnSessionChange;
            OnConfigReloadHandler += OnConfigReload;
            OnAgentResponseHandler += OnAgentResponse;
            OnMQTTConnectedHandler += OnMQTTConnected;
            OnMQTTDisconnectedHandler += OnMQTTDisconnected;
            OnMQTTMessageReceivedHandler += OnMQTTMessageReceived;
            OnRefreshRequestedHandler += OnRefreshRequested;
        }

        private void OnRefreshRequested(object sender, System.EventArgs e)
        {
            /**
             * This method is called when the user requested that core and
             * all addons refresh and resend all available information.
             * 
             * Here you should clear cache, resend all information to the
             * MQTT topics you publish to.
             **/
            throw new System.NotImplementedException();
        }

        private void OnMQTTMessageReceived(object sender, IOTLinkAPI.Platform.Events.MQTT.MQTTMessageEventEventArgs e)
        {
            /**
             * This event is called when your addon receives a MQTT message
             **/
            throw new System.NotImplementedException();
        }

        private void OnMQTTDisconnected(object sender, IOTLinkAPI.Platform.Events.MQTT.MQTTEventEventArgs e)
        {
            /**
             * This event is called when there is a MQTT disconnection.
             **/
            throw new System.NotImplementedException();
        }

        private void OnMQTTConnected(object sender, IOTLinkAPI.Platform.Events.MQTT.MQTTEventEventArgs e)
        {
            /**
             * This event is called when there is a MQTT connect.
             **/
            throw new System.NotImplementedException();
        }

        private void OnSessionChange(object sender, SessionChangeEventArgs e)
        {
            /**
             * This event is called when OS (Windows) inform the service that
             * the current user has changed his session state.
             * 
             * Useful if you need to handle lock/unlock/logon/logoff/etc.
             **/
            throw new System.NotImplementedException();
        }

        private void OnConfigReload(object sender, ConfigReloadEventArgs e)
        {
            /**
             * IOT Link call this event when there is a system configuration change.
             * There are two types of changes:
             * - Engine: User has changed main configuration file.
             * - Addon: User has changed any configuration you used the method
             * ConfigHelper.SetReloadHandler()
             **/
            switch (e.ConfigType)
            {
                case ConfigType.CONFIGURATION_ENGINE:
                    LoggerHelper.Debug("OnConfigReload - Engine Config Reload Detected!");
                    break;

                case ConfigType.CONFIGURATION_ADDON:
                    LoggerHelper.Debug("OnConfigReload - Addon Config Reload Detected!");
                    _config = ConfigHelper.GetConfiguration<MyConfigClass>(_configPath);
                    break;
            }
        }

        private void OnAgentResponse(object sender, AgentAddonResponseEventArgs e)
        {
            /**
             * This event is called when the agent has replied your 
             * GetManager().SendAgentRequest() call.
             **/
            LoggerHelper.Debug("OnAgentResponse: {0}", e.Data);

            AddonRequestType requestType = e.Data.requestType;
            switch (requestType)
            {
                case AddonRequestType.REQUEST_MY_REQUEST:
                    // Do Something
                    break;

                default: break;
            }
        }

        private void RequestAgentSomething()
        {
            /**
             * This is an example of how can you call your Agent Addon.
             **/
            dynamic addonData = new ExpandoObject();
            addonData.requestType = AddonRequestType.REQUEST_MY_REQUEST;

            GetManager().SendAgentRequest(this, addonData);
        }
    }
}
